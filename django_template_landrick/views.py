class MenuGroup(object):
    def __init__(
        self, menu_name="whatever", menu_items=None, menu_icon="folder", link_url=None
    ):
        if menu_items is None:
            menu_items = list()

        self.menu_name = menu_name
        self.menu_items = menu_items
        self.menu_icon = menu_icon
        self.link_url = link_url

    def has_items(self):
        if len(self.menu_items) != 0:
            return True
        else:
            return False


class MenuLink(object):
    def __init__(self, link_name="whatever", link_url="/dashboard/"):
        self.link_name = link_name
        self.link_url = link_url
